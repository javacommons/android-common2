package jc.android.common;

import android.app.Service;
import android.os.Handler;
import android.widget.Toast;

public abstract class CommonService extends Service {
    private Handler toastHandler = new Handler();
    protected void dbgmsg(String msg) {
        toastHandler.post(new Runnable() {
            public void run() {
                Toast.makeText(CommonService.this, msg, Toast.LENGTH_SHORT).show();
            }
        });
    }
}
